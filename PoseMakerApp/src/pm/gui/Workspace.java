package pm.gui;

import pm.controller.PageEditController;
import java.io.IOException;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
// Import javafx controls
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import pm.PoseMaker;
import pm.PropertyType;
import pm.data.DataManager;
import pm.file.FileManager;


/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @coauthor Han Zhao 
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {
    static final int BUTTON_TAG_WIDTH = 40;
    static final String SELECT = "Select";
    static final String DELETE = "Delete";
    static final String RECTANGLE = "Rectangle";
    static final String ELLIPSE = "Ellipse";
    static final String BACK = "Back";
    static final String FORWARD = "Front";
    static final String SNAPSHOT = "Snapshot";
    
    // CSS CLASSES
    static final String CLASS_TOOLBAR = "max_pane";
    static final String CLASS_SUBTOOLBAR = "bordered_pane";
    static final String CLASS_LABELFONT = "prompt_label";
    
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    
    // THIS HANDLES INTERACTIONS WITH PAGE EDITING CONTROLS
    PageEditController pageEditController;

    // CREATES BORDERPANE WHICH HANDLES LAYOUT OF WORKSPACE
    BorderPane border;
    
    // MAIN TOOLBAR ON LEFT SIDE
    VBox toolbar;
    
    // SUBTOOLBARS UNDER THE MAIN TOOLBAR
    HBox modifyToolbar;
    HBox backFrontToolbar;
    VBox backgroundColorToolbar;
    VBox fillColorToolbar;
    VBox outlineColorToolbar;
    VBox outlineThicknessToolbar;
    HBox snapshotToolbar;
    
    // CANVAS PANE
    Pane canvasPane;
    
    // BUTTONS FOR RESPECTIVE SUBTOOLBARS
    Button select;
    Button remove;
    Button rectangle;
    Button ellipse;
    
    Button moveToBack;
    Button moveToFront;
    
    Button snapshot;
    
    // COLOR PICKERS FOR PICKING COLORS
    ColorPicker background;
    ColorPicker fill;
    ColorPicker outline;
    
    // SLIDER FOR OUTLINE THICKNESS
    Slider outlineThickness;
    
    String currentTool;
    
    DataManager dataManager;
    FileManager fileManager;
    
    // TEXT FOR LABELING
    Text backgroundText;
    Text fillText;
    Text outlineText;
    Text outlineThicknessText;
    
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;
	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        Scene scene = gui.getPrimaryScene();
        
        dataManager = (DataManager) app.getDataComponent();
        fileManager = (FileManager)app.getFileComponent();
        pageEditController = new PageEditController((PoseMaker) app);
        // NEED TO ADD IN CONSTUCTOR FOR PAGE EDIT CONTROLLER
        // TESTING FOR GUI NOW
        
        
        //dataManager= (DataManager)app.getDataComponent();
        //PropertiesManager propsSingleton = PropertiesManager.getPropertiesManager();
        
        // ADD BUTTONS
        border = new BorderPane();
        border.prefHeightProperty().bind(scene.heightProperty());
        border.prefWidthProperty().bind(scene.widthProperty());
        toolbar = new VBox();
        toolbar.setPadding(new Insets(10, 50, 50, 50));
        toolbar.setSpacing(10);
        modifyToolbar = new HBox();
        
        // Create Select Button
        select = gui.initChildButton(modifyToolbar, 
                PropertyType.SELECTION_TOOL_ICON.toString(), 
                PropertyType.SELECTION_TOOL_TOOLTIP.toString(), 
                false);
        select.setMaxWidth(BUTTON_TAG_WIDTH);
	select.setMinWidth(BUTTON_TAG_WIDTH);
	select.setPrefWidth(BUTTON_TAG_WIDTH);
        select.setOnAction(e -> {
           canvasPane.setCursor(Cursor.DEFAULT);
           currentTool = SELECT; 
        });
        
        // Create Remove Button
        remove = gui.initChildButton(modifyToolbar, 
                PropertyType.REMOVE_ICON.toString(), 
                PropertyType.REMOVE_TOOLTIP.toString(), 
                false);
        remove.setMaxWidth(BUTTON_TAG_WIDTH);
	remove.setMinWidth(BUTTON_TAG_WIDTH);
	remove.setPrefWidth(BUTTON_TAG_WIDTH);
        remove.setOnAction(e -> {
            if (dataManager.getCurrentShape() != null) {
                pageEditController.removeShapeRequest(dataManager.getCurrentShape());
                currentTool = "";
            }
        });
        
        // Create Rectangle Button
        rectangle = gui.initChildButton(modifyToolbar, 
                PropertyType.RECTANGLE_ICON.toString(), 
                PropertyType.RECTANGLE_TOOLTIP.toString(), 
                false);
        rectangle.setMaxWidth(BUTTON_TAG_WIDTH);
	rectangle.setMinWidth(BUTTON_TAG_WIDTH);
	rectangle.setPrefWidth(BUTTON_TAG_WIDTH);
        rectangle.setOnAction(e-> {
            if (dataManager.getCurrentShape() != null) {
                dataManager.getCurrentShape().setStroke(dataManager.getCurrentShapeColor());
                dataManager.setCurrentShapeColor(null);
                dataManager.setCurrentShape(null);
            }
            canvasPane.setCursor(Cursor.CROSSHAIR);
            currentTool = RECTANGLE;
        });
        
        // Create Circle Button
        ellipse = gui.initChildButton(modifyToolbar, 
                PropertyType.ELLIPSE_ICON.toString(), 
                PropertyType.ELLIPSE_TOOLTIP.toString(), 
                false);
        ellipse.setMaxWidth(BUTTON_TAG_WIDTH);
	ellipse.setMinWidth(BUTTON_TAG_WIDTH);
	ellipse.setPrefWidth(BUTTON_TAG_WIDTH);
        ellipse.setOnAction(e -> {
            if (dataManager.getCurrentShape() != null) {
                dataManager.getCurrentShape().setStroke(dataManager.getCurrentShapeColor());
                dataManager.setCurrentShapeColor(null);
                dataManager.setCurrentShape(null);
            }
            canvasPane.setCursor(Cursor.CROSSHAIR);
            currentTool = ELLIPSE;
        });
        
        // Create Movement toolbar
        backFrontToolbar = new HBox();
        
        // Create up and down buttons
        moveToBack = gui.initChildButton(backFrontToolbar, 
                PropertyType.MOVETOBACK_ICON.toString(), 
                PropertyType.MOVETOBACK_TOOLTIP.toString(), 
                false);
        moveToBack.setMaxWidth(BUTTON_TAG_WIDTH);
	moveToBack.setMinWidth(BUTTON_TAG_WIDTH+50);
	moveToBack.setPrefWidth(BUTTON_TAG_WIDTH);
        moveToBack.setOnAction(e -> {
            if (currentTool.equals(SELECT)) {
                pageEditController.changeShapePositionRequest(BACK);
            }
        });
        
        moveToFront = gui.initChildButton(backFrontToolbar, 
                PropertyType.MOVETOFRONT_ICON.toString(), 
                PropertyType.MOVETOFRONT_TOOLTIP.toString(), 
                false);
        moveToFront.setMaxWidth(BUTTON_TAG_WIDTH);
	moveToFront.setMinWidth(BUTTON_TAG_WIDTH+50);
	moveToFront.setPrefWidth(BUTTON_TAG_WIDTH);
        moveToFront.setOnAction(e -> {
            if (currentTool.equals(SELECT)) {
                pageEditController.changeShapePositionRequest(FORWARD);
            }
        });
        
        // Create Background Color Toolbar
        backgroundColorToolbar = new VBox();
        
        // Create it's color picker
        background = new ColorPicker();
        backgroundText = new Text("Background Color");
        backgroundColorToolbar.getChildren().addAll(backgroundText, background);
        background.setValue(Color.BEIGE);
        dataManager.setBackground(background.getValue());
        background.setOnAction(e-> {
            pageEditController.setBackgroundRequest(background.getValue());
        });
        
        // Create Fill Color Toolbar
        fillColorToolbar = new VBox();
        
        // Create it's color picker
        fill = new ColorPicker();
        fillText = new Text("Fill Color");
        fillColorToolbar.getChildren().addAll(fillText, fill);
        fill.setOnAction(e -> {
            if (currentTool.equals(SELECT)) {
                dataManager.getCurrentShape().setFill(fill.getValue());
            }
        });
        
        // Create Outline Color Toolbar
        outlineColorToolbar = new VBox();
        outline = new ColorPicker();
        outlineText = new Text("Outline Color");
        outlineColorToolbar.getChildren().addAll(outlineText, outline);
        outline.setOnAction(e -> {
            if (currentTool.equals(SELECT)) {
                dataManager.setCurrentShapeColor(outline.getValue());
            }
        });
        
        // Create Outline Thickness Toolbar
        outlineThicknessToolbar = new VBox();
        
        // Create it's slider
        outlineThickness = new Slider(1, 20, 10);
        outlineThicknessText = new Text("Outline Thickness");
        outlineThicknessToolbar.getChildren().addAll(outlineThicknessText, outlineThickness);
        outlineThickness.setOnMouseDragged(e -> {            
            if (currentTool.equals(SELECT)) {
                dataManager.getCurrentShape().setStrokeWidth(outlineThickness.getValue());
                reloadWorkspace();
            }
        });
        
        snapshotToolbar = new HBox();
        
        // Finally, create snapshot button
        snapshot = gui.initChildButton(toolbar, 
                PropertyType.SNAPSHOT_ICON.toString(), 
                PropertyType.SNAPSHOT_TOOLTIP.toString(), 
                false);
        snapshot.setOnAction(e -> {
            
            fileManager.snapshot(scene);
        });
        snapshotToolbar.getChildren().addAll(snapshot);
        
        // ADD ALL SUBTOOLBARS TO MAIN TOOLBAR
        toolbar.getChildren().addAll(modifyToolbar, 
                backFrontToolbar,
                backgroundColorToolbar,
                fillColorToolbar,
                outlineColorToolbar,
                outlineThicknessToolbar,
                snapshotToolbar);
        

        
        
        
        
        canvasPane = new Pane();
        canvasPane.setStyle("-fx-border-color: Black");
        
        canvasPane.setOnMousePressed(e -> {
            if (currentTool.equals(RECTANGLE)) {
                
                pageEditController.handleAddShape(e.getX(), e.getY(), outlineThickness.getValue(), outline.getValue(), fill.getValue(), RECTANGLE);
            } else if (currentTool.equals(ELLIPSE)) {
                canvasPane.setCursor(Cursor.CROSSHAIR);
                pageEditController.handleAddShape(e.getX(), e.getY(), outlineThickness.getValue(), outline.getValue(), fill.getValue(), ELLIPSE);
            } else if (currentTool.equals(SELECT)) {
                canvasPane.setCursor(Cursor.HAND);
                pageEditController.find(e.getX(), e.getY());
                fill.setValue((Color)dataManager.getCurrentShape().getFill());
                outline.setValue((Color)dataManager.getCurrentShapeColor());
                outlineThickness.setValue(dataManager.getCurrentShape().getStrokeWidth());
                pageEditController.setShapeRequest(e.getX(), e.getY());
            }
        });
        
        canvasPane.setOnMouseDragged(e -> { 
            if (currentTool.equals(RECTANGLE)) {
                pageEditController.resizeNewShapeRequest(e.getX(),e.getY());
            } else if (currentTool.equals(ELLIPSE)) {
                pageEditController.resizeNewShapeRequest(e.getX(),e.getY());
            } else if (currentTool.equals(SELECT)) {
                pageEditController.moveShapeRequest(e.getX(), e.getY()); 
            }
        });
        
        canvasPane.setOnMouseReleased(e -> {
            if (currentTool.equals(RECTANGLE) || currentTool.equals(ELLIPSE)) {;
            } else if (currentTool.equals(SELECT)) {
                pageEditController.setWithinBorder(false); 
                canvasPane.setCursor(Cursor.DEFAULT);
            }
        });
        

        
       
        border.setLeft(toolbar);
        border.setCenter(canvasPane);
        
        workspace = new Pane();
        workspace.getChildren().add(border);

        System.out.println(1);
        initStyle();
    }
    
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        
        // CSS FORMAT THE SUBTOOLBARS
        modifyToolbar.getStyleClass().add(CLASS_SUBTOOLBAR);
        backFrontToolbar.getStyleClass().add(CLASS_SUBTOOLBAR);
        backgroundColorToolbar.getStyleClass().add(CLASS_SUBTOOLBAR);
        fillColorToolbar.getStyleClass().add(CLASS_SUBTOOLBAR);
        outlineColorToolbar.getStyleClass().add(CLASS_SUBTOOLBAR);
        outlineThicknessToolbar.getStyleClass().add(CLASS_SUBTOOLBAR);
        snapshotToolbar.getStyleClass().add(CLASS_SUBTOOLBAR);
        
        // NOW FORMAT THE MAIN TOOLBAR
        toolbar.getStyleClass().add(CLASS_TOOLBAR);
        
        // NOW FORMAT THE TEXT
        backgroundText.getStyleClass().add(CLASS_LABELFONT);
        fillText.getStyleClass().add(CLASS_LABELFONT);
        outlineText.getStyleClass().add(CLASS_LABELFONT);
        outlineThicknessText.getStyleClass().add(CLASS_LABELFONT);
        
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
        try {
            pageEditController.enable(false); 

            dataManager = (DataManager) app.getDataComponent();
            ArrayList<Shape> shapes = dataManager.getShapes();

            // Clear current shapes
            canvasPane.getChildren().clear();
            
            // Redraw the background
            if (dataManager.getBackground() != null) {
                String color = dataManager.getHex(dataManager.getBackground());
                canvasPane.setStyle("-fx-background-color: " + color);
            }

            // Redraw the shapes
            for (Shape shape : shapes) {
                canvasPane.getChildren().add(shape);
            }
            
            pageEditController.enable(true);
            
        } catch (Exception e) {

        }
    }
}
