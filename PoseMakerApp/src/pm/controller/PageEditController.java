/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.controller;

import java.util.ArrayList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import pm.PoseMaker;
import pm.data.DataManager;
import pm.gui.Workspace;

/**
 *
 * @author Han Zhao
 */
public class PageEditController {
    
    PoseMaker app;
    
    private boolean enabled;
    
    private double oriX;
    private double oriY;

    public void enable(boolean b) {
        enabled = b;
    }
    
    public PageEditController(PoseMaker initApp) {
	// KEEP IT FOR LATER
	app = initApp;
        enabled = true;
    }


        
    /**
     * Adds shape to existing list of shapes.
     * 
     * @param xCor x coordinate of shape
     * @param yCor y coordinate of shape
     * @param strokeWidth width of the stroke
     * @param strokeColor color of the border
     * @param fillColor color of the shape
     * @param shape name of shape
     */
    public void handleAddShape(double xCor, double yCor, double strokeWidth, Color strokeColor, Color fillColor, String shape) {
        if (enabled) {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            Shape insertedShape;
            if (shape.equals("Rectangle")) {
                insertedShape = new Rectangle(xCor, yCor, 0, 0);
                insertedShape.setStrokeWidth(strokeWidth);
                insertedShape.setStroke(strokeColor);
                insertedShape.setFill(fillColor);
                oriX = xCor;
                oriY = yCor;
                dataManager.addShape(insertedShape);
            } else if (shape.equals("Ellipse")) {
                insertedShape = new Ellipse(xCor, yCor, 0, 0);
                insertedShape.setStrokeWidth(strokeWidth);
                insertedShape.setStroke(strokeColor);
                insertedShape.setFill(fillColor);
                dataManager.addShape(insertedShape);
                
            }
            workspace.reloadWorkspace();
        }
    }

    /**
     * Removes shape off shapes in DataManager
     * 
     * @param deletedShape Shape that is to be deleted
     */
    public void removeShapeRequest(Shape deletedShape) {
        if (enabled) {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            
            ArrayList<Shape> shapes = dataManager.getShapes();
            shapes.remove(deletedShape);
            if (dataManager.getCurrentShape() != null) {
                dataManager.setCurrentShape(null);
                dataManager.setCurrentShapeColor(null);
            }
            workspace.reloadWorkspace();
        }
    }
    
    /**
     * Handles a change in shape properties
     * 
     * @param xCor x coordinate of shape to be changed
     * @param yCor y coordinate of shape to be changed
     * @param fillColor fill color to be changed
     * @param outlineColor outline color to be changed
     * @param thickness outline thickness to be changed
     */
    public void changeShapeRequest(double xCor, double yCor, Color fillColor, Color outlineColor, double thickness) {
        if (enabled) {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            
            ArrayList<Shape> shapes = dataManager.getShapes();
            for (int i = shapes.size()-1; i >= 0; i--) {
                Shape shape = shapes.get(i);
                if (shape.contains(xCor, yCor)) {
                    if (fillColor != null) {
                        shape.setFill(fillColor);
                    } else if (outlineColor != null) {
                        shape.setStroke(outlineColor);
                    } else if (thickness != -1) {
                        shape.setStrokeWidth(thickness);
                    }
                    return;
                }
            }            
            workspace.reloadWorkspace();
        }
    }
    
    public void changeShapePositionRequest(String frontOrBack) {
        if (enabled) {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            
            ArrayList<Shape> shapes = dataManager.getShapes();
            if (frontOrBack.equals("Front")) {
                System.out.println(3);
                shapes.remove(dataManager.getCurrentShape());
                shapes.add(dataManager.getCurrentShape());
                System.out.println(2);
            } else if (frontOrBack.equals("Back")) {
                shapes.remove(dataManager.getCurrentShape());
                shapes.add(0, dataManager.getCurrentShape());
            }
            workspace.reloadWorkspace();
        }
    }
    
    public void resizeNewShapeRequest(double newX, double newY) {
        if (enabled) {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            
            ArrayList<Shape> shapes = dataManager.getShapes();
            Shape shape = shapes.get(shapes.size()-1);
            if (shape instanceof Rectangle) {
                Rectangle shapeRec = (Rectangle) shape;
                if (Math.min(oriX, newX) > 0 && Math.min(oriY, newY) > 0) {
                    shapeRec.setX(Math.min(oriX, newX));
                    shapeRec.setY(Math.min(oriY, newY));
                    shapeRec.setWidth(Math.abs(oriX-newX));
                    shapeRec.setHeight(Math.abs(oriY-newY));
                }
            } else if (shape instanceof Ellipse) {
                Ellipse shapeEli = (Ellipse) shape;
                shapeEli.setRadiusX(Math.abs(shapeEli.getCenterX() - newX));
                shapeEli.setRadiusY(Math.abs(shapeEli.getCenterY() - newY));
            workspace.reloadWorkspace();
            }
        }
    }
    
    public void setBackgroundRequest(Color backgroundColor) {
        if (enabled) {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            
            dataManager.setBackground(backgroundColor);
            workspace.reloadWorkspace();
        }
    }
    
    private double xTranslation;
    private double yTranslation;
    private boolean withinBorder;
    
    public void setShapeRequest(double newX, double newY) {
        if (enabled) {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            
            Shape toMove = dataManager.getCurrentShape();
            if (toMove.contains(newX, newY)) {
                withinBorder = true;
                if (toMove instanceof Rectangle) {
                    Rectangle toMoveRec = (Rectangle) toMove;
                    xTranslation = newX - toMoveRec.getX();
                    yTranslation = newY - toMoveRec.getY();
                } else if (toMove instanceof Ellipse) {
                    Ellipse toMoveEll = (Ellipse) toMove;
                    xTranslation = toMoveEll.getCenterX() - newX;
                    yTranslation = toMoveEll.getCenterY() - newY;
                }
                System.out.println(xTranslation + " " + yTranslation);
            }
            workspace.reloadWorkspace();
        }
    }
    
    public void moveShapeRequest(double newX, double newY) {
        if (enabled && withinBorder) {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            
            Shape toMove = dataManager.getCurrentShape();
            if (toMove instanceof Rectangle) {
                Rectangle toMoveRec = (Rectangle) toMove;
                toMoveRec.setX(newX - xTranslation);
                toMoveRec.setY(newY - yTranslation);
            } else if (toMove instanceof Ellipse) {
                Ellipse toMoveEll = (Ellipse) toMove;
                toMoveEll.setCenterX(newX + xTranslation);
                toMoveEll.setCenterY(newY + yTranslation);
            }
            workspace.reloadWorkspace();
        }
    }
    
    public void setWithinBorder(boolean n) {
        withinBorder = n;
    }
    
    public Shape find(double xCor, double yCor) {
        if (enabled) {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            
            ArrayList<Shape> shapes = dataManager.getShapes();
            for (int i = shapes.size()-1; i >= 0; i--) {
                Shape shape = shapes.get(i);
                if (shape.contains(xCor, yCor)) {
                    if (dataManager.getCurrentShape() != null) {
                        dataManager.getCurrentShape().setStroke(dataManager.getCurrentShapeColor());
                    }
                    
                    dataManager.setCurrentShapeColor((Color)shape.getStroke());
                    dataManager.setCurrentShape(shape);
                    shape.setStroke(Color.YELLOW);
                    workspace.reloadWorkspace();
                    return shape;
                }
            }
        }
        return null;
    }
}
