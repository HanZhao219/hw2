package pm.data;

import java.util.ArrayList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @coauthor Han Zhao 
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    Shape currentShape;
    Color currentShapeColor;
    Color background;
    // ARRAY OF IMAGES TO BE USED
    ArrayList<Shape> shapes;
    
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        
        shapes = new ArrayList();
    }
    
    public void addShape(Shape shape) {
        shapes.add(shape);
    }
    
    /**
     * Accessor for getting all the shapes
     * 
     * @return A list of all the shapes that are currently drawn
     */
    public ArrayList<Shape> getShapes() {
        return shapes;
    }
    
    public String getHex(Color color) {
        String hexValue = String.format( "#%02X%02X%02X",
                (int)( color.getRed() * 255 ),
                (int)( color.getGreen() * 255 ),
                (int)( color.getBlue() * 255 ) );
        return hexValue;
    }
    
    public Color getColor(final String rgbString) {
        String rgbStr = rgbString;
        if (rgbString.charAt(0) == '#') {
            rgbStr = rgbString.substring(1);
        }
        final int[] rgb = new int[3];
        for (int i = 0; i < 3; i++)
        {
            rgb[i] = Integer.parseInt(rgbStr.substring(i * 2, i * 2 + 2), 16);
        }
        Color returnColor = new Color((double)rgb[0]/255, (double)rgb[1]/255, (double)rgb[2]/255, 1.0);
        return returnColor;
    }
    
    public Shape getCurrentShape() {
        return currentShape;
    }
    
    public void setCurrentShape(Shape shape) {
        currentShape = shape;
    }
            
    public Color getCurrentShapeColor() {
        return currentShapeColor;
    }
    
    public void setCurrentShapeColor(Color shapeColor) {
        currentShapeColor = shapeColor;
    }

    public Color getBackground() {
        return background;
    }
    
    public void setBackground(Color back) {
        background = back;
    }
    
    
    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        shapes = new ArrayList();
    }

}
