package pm.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import pm.data.DataManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @coauthor Han Zhao 
 * @version 1.0
 */
public class FileManager implements AppFileComponent {
    final static String FILE_PATH = "work";
    final static String JSON_BACKGROUND_COLOR = "Background";
    final static String JSON_SHAPES = "Shapes";
    final static String JSON_SHAPE = "Shape";
    final static String JSON_FILL = "Fill";
    final static String JSON_STROKE = "Stroke";
    final static String JSON_STROKE_WIDTH = "Stroke Width";
    final static String JSON_XCOR = "Xcor";
    final static String JSON_YCOR = "Ycor";
    final static String JSON_HEIGHT = "Height";
    final static String JSON_WIDTH = "Width";
    
    public void snapshot(Scene scene) {
        WritableImage image = new WritableImage((int) scene.getWidth(), (int) scene.getHeight());
        scene.snapshot(image);
        
        File file = new File(FILE_PATH + "/Pose.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            System.out.println("Snapshot saved to test folder.");
        } catch (Exception e) {
        }
    }
    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        System.out.println(1);
        StringWriter sw = new StringWriter();
        
        DataManager dataManager = (DataManager) data;
        // Turn Shapes into 2D array
        
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        
        for (Shape shape : dataManager.getShapes()) {
            JsonObjectBuilder obj = Json.createObjectBuilder();
            if (shape instanceof Rectangle) {
                Rectangle rec = (Rectangle) shape;
                obj.add("Shape", "Rectangle");
                obj.add("Xcor", rec.getX());
                obj.add("Ycor", rec.getY());
                obj.add("Height", rec.getHeight());
                obj.add("Width", rec.getWidth());
            } else if (shape instanceof Ellipse) {
                Ellipse elli = (Ellipse) shape;
                obj.add("Shape", "Ellipse");
                obj.add("Xcor", elli.getCenterX());
                obj.add("Ycor", elli.getCenterY());
                obj.add("Height", elli.getRadiusY());
                obj.add("Width", elli.getRadiusX());
            }
            if (dataManager.getCurrentShape()==shape) {
                obj.add("Stroke", dataManager.getHex(dataManager.getCurrentShapeColor()));
            } else {
                obj.add("Stroke", dataManager.getHex((Color)shape.getStroke()));
            }
            obj.add("Fill", dataManager.getHex((Color)shape.getFill()));
            
            obj.add("Stroke Width", shape.getStrokeWidth());
            JsonObject node = obj.build();
            arrayBuilder.add(node);
        }
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add("Background", dataManager.getHex(dataManager.getBackground()))
                .add("Shapes", arrayBuilder)
                .build();
        
        
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
        
        
        
    }
    
      
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
        
        DataManager dataManager = (DataManager) data;
        
        dataManager.reset();
        // GET THE ARRAY
	JsonArray shapesArray = json.getJsonArray(JSON_SHAPES);
        
        // LOAD ALL ITEMS IN
        for (int i = 0; i<shapesArray.size(); i++) {
            JsonObject shapeJSO = shapesArray.getJsonObject(i);
            Shape node;
            if (shapeJSO.getString(JSON_SHAPE).equals("Rectangle")) {
                node = new Rectangle(shapeJSO.getInt(JSON_XCOR), 
                        shapeJSO.getInt(JSON_YCOR), 
                        shapeJSO.getInt(JSON_WIDTH), 
                        shapeJSO.getInt(JSON_HEIGHT));
                node.setFill(dataManager.getColor(shapeJSO.getString(JSON_FILL)));
                node.setStroke(dataManager.getColor(shapeJSO.getString(JSON_STROKE)));
                node.setStrokeWidth(shapeJSO.getInt(JSON_STROKE_WIDTH));
                dataManager.addShape(node);
            } else if (shapeJSO.getString(JSON_SHAPE).equals("Ellipse")) {
                node = new Ellipse(shapeJSO.getInt(JSON_XCOR), 
                        shapeJSO.getInt(JSON_YCOR), 
                        shapeJSO.getInt(JSON_WIDTH), 
                        shapeJSO.getInt(JSON_HEIGHT));
                node.setFill(dataManager.getColor(shapeJSO.getString(JSON_FILL)));
                node.setStroke(dataManager.getColor(shapeJSO.getString(JSON_STROKE)));
                node.setStrokeWidth(shapeJSO.getInt(JSON_STROKE_WIDTH));
                dataManager.addShape(node);
            }
        }
        dataManager.setBackground(dataManager.getColor(json.getString(JSON_BACKGROUND_COLOR)));
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {

    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }
}
